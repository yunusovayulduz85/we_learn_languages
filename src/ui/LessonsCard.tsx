import { Avatar, Box, Button, Card, CardActions, CardContent, Typography } from "@mui/material"
import { ItemType } from "../types/lesson"
import { Link } from "react-router-dom"
import { deepOrange } from "@mui/material/colors";
import { Dispatch, SetStateAction, useState } from "react";
import VerifiedIcon from '@mui/icons-material/Verified';
  type Props = {
    lesson:ItemType,
    check:boolean,
    setCheckId:Dispatch<SetStateAction<number>>
  }
  const LessonCard:React.FC<Props>  = ({lesson,check,setCheckId}) => {  
  const ChangeCheck = (id:number) =>{
    setCheckId(id)
  }
  const quizId = localStorage.getItem("quizId")
  const quizIdNum = Number(quizId)
  const score = localStorage.getItem('score')
  const scoreNum = Number(score)
  const result = (scoreNum/10)*100;
  return <>
      
        <Card sx={{mt:1,width:"350px",display:'flex',flexDirection:'column',boxShadow:3,background:"#f2e4cf",justifyContent:'space-between'}}>
             <CardContent>
               <Box sx={{display:'flex',justifyContent:'end'}}>
                  <Typography variant="body2" sx={{color:'#00a152'}}>
                    {
                      quizIdNum === lesson.id && (lesson.lang === "eng" && `Appropriation : ${result}%` || lesson.lang === "rus" && `Aссигнования : ${result}%` || lesson.lang === "uzb" && `O'zlashtirish : ${result}%`)
                    }
                  </Typography>
                </Box>
               <Box sx={{display:'flex',justifyContent:'center',mb:"10px"}}>
                    <Avatar sx={{ bgcolor: deepOrange[500] }}>{lesson.id}</Avatar>
                </Box>
               
               <Typography variant='h5' sx={{textAlign:'center',color:"red"}}>
               {lesson.lessonName} 
                </Typography>
               <Typography variant='body1' sx={{textAlign:'center'}}>
                {lesson.description}
                </Typography>
               <Typography variant='h6' sx={{textAlign:'center',color:"#966721"}}>
                 {lesson.level}
                </Typography>
             </CardContent>
             <CardActions sx={{display:'flex',justifyContent:'center'}}>
              <Link to={`${lesson.id}`}>
              <Button
               variant='contained'
               color='warning'
               onClick={()=>ChangeCheck(lesson.id)}
              >
                 {lesson.lang === "eng" && "Start Lesson"}
                 {lesson.lang === "rus" && "Начать урок"}
                 {lesson.lang === "uzb" && "Darsni boshlash"}
              </Button>
              </Link>
              {
                   check && <VerifiedIcon/>                               
              }

             </CardActions>
         </Card>
  </>
}

export default LessonCard