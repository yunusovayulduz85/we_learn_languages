import { Box, Button, Card, CardActions, CardContent, Typography } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router'

const PersonalData = () => {
    const name= localStorage.getItem("name")
    const email = localStorage.getItem("email")
    const getPassword : string | null = localStorage.getItem("password")
    const getPasswordNum : number | null = getPassword !== null ? Number(getPassword) : null
    const navigation = useNavigate()
    const handleClick = () =>{
        navigation('/login')
    }
  return (
    <Box sx={{display:'flex',justifyContent:'center',mt:"50px"}}>
        <Card sx={{width:"500px"}}>
            <CardContent>
            <Typography variant='h5' sx={{textAlign:'center',color:'#651fff'}}>Your Information</Typography>
            <Typography sx={{textAlign:'center',color:'#651fff'}}>userName : {name}</Typography>
            <Typography sx={{textAlign:'center',color:'#651fff'}}>email : {email}</Typography>
            <Typography sx={{textAlign:'center',color:'#651fff'}}>password : {getPasswordNum}</Typography>
            </CardContent>
            <CardActions sx={{display:'flex',justifyContent:'center'}}>
            <Button variant='contained' onClick={handleClick}>Back</Button>
            </CardActions>
        </Card>
    </Box>
  )
}

export default PersonalData