import React, { useState } from 'react'
import { Skeleton, Typography} from '@mui/material'
import LessonCard from '../../ui/LessonsCard'
import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { ItemType } from '../../types/lesson'
const EnglishLesson:React.FC = () => {
  const [engCheck, setEngCheck] = useState<boolean>(false)
  const getEnglishLesson = localStorage.getItem("english")
  const getEnglishLessonArr = getEnglishLesson? JSON.parse(getEnglishLesson):[]
  const [checkId, setCheckId]=useState<number>(0)
  return <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         All Lessons
      </Typography>
  <LessonCardWrapper>
    
        {
        getEnglishLessonArr?.map((item:ItemType , index:number) => {
          if( checkId === item.id) setEngCheck(true);
        return<>
          <LessonCard 
        lesson={item}
        check={engCheck}
        setCheckId={setCheckId}
        key={index}  />
        </>
         })
        }
      </LessonCardWrapper>   
  </>
   
}

export default EnglishLesson