import React, { useState } from 'react'
import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { Button, Card, CardActions, CardContent, Skeleton, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import { ItemType } from '../../types/lesson'
import LessonCard from '../../ui/LessonsCard'
let uzbCheck : boolean = false 
const UzbekLesson = () => {
  const getUzbekLesson = localStorage.getItem("uzbek")
  const getUzbekLessonArr = getUzbekLesson? JSON.parse(getUzbekLesson):[]
  const [checkId, setCheckId] = useState<number>(0)
  return (
    <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         Barcha Darslar
      </Typography>
    <LessonCardWrapper>
      {
       getUzbekLessonArr?.map((item:ItemType , index:number) => {
        if( checkId === item.id) uzbCheck = true;
        return<>
        <LessonCard 
        lesson={item}
        check={uzbCheck}
        setCheckId={setCheckId}
        key={index}
        />
        </>
         })
        }
     </LessonCardWrapper>
    </>
     
  )
}

export default UzbekLesson