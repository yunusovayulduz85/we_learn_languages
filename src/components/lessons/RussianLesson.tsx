import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { Skeleton, Typography} from '@mui/material'
import { ItemType } from '../../types/lesson'
import LessonCard from '../../ui/LessonsCard'
import { useState } from 'react'
let rusCheck : boolean = false 

const RussionLesson = () => {
  const getRussianLesson = localStorage.getItem("russian")
  const getRussianLessonArr = getRussianLesson? JSON.parse(getRussianLesson):[]
  const [checkId, setCheckId] = useState<number>(0)

  return (
   <>
    <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         Все уроки
      </Typography>
   <LessonCardWrapper>
      {
        getRussianLessonArr?.map((item:ItemType , index:number) => {
       if( checkId === item.id) rusCheck = true;
        return<>
        <LessonCard 
         lesson={item}
         check={rusCheck}
         setCheckId={setCheckId}
         key={index}
        />
        </>
         })
        }
     
     </LessonCardWrapper>
   </>
   
  )
}

export default RussionLesson